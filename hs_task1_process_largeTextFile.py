"""
******
TASK 1
******

Objectives:
1) In this task, given a large text file (few GBs or TBs), we have to remove
    StopWords from the file.
2) Find most common and least common k words.

To achieve (1), we have defined a MAX_FILE_LIMIT, which denotes the maximum
size of the file, which can be handled in-memory. We shard the large file,
into multiple files of MAX_FILE_LIMIT. We initiate THREADS to handle each
sharded file. Also, within each shard, file is read and processed line by
line, so memory consumption stays within limit. A new file is created
corresponding to each sharded file, with stopWords Removed.

To achieve (2), while processing each shard, we keep a track of words
frequency within current file. Just before exiting the thread, we update
"globalWordsDict" with the words frequency from this file. "globalWordsDict"
keeps track of words frequency amongst all thread. We take care to use
"mutex locks", so that two threads dont end up updating global dictionary
at the same time.

Once we have word frequencies for all words, we have 2 options to find most
commonly appearing words. Either sort the the dictionary and take the top
(or bottom) k words [complexity- O(nlog(n))]. Or use a heap to maintain only
top k words. [complexity- O(nlog(k))] Although heap method is more optimized,
but it will only matter if the word count is very high. Since english language
contain atmost 400,00 words, performance gain wont be very high.

"""
import math
import sys
import os
import operator
import threading
import heapq
from itertools import chain, islice
from collections import Counter

# Max number of lines for the smaller shards
MAX_FILE_LIMIT = 5000

# Dict of words to be scanned and removed
STOPWORDS = {}

# Top n words by frequency
TOP_N_WORDS = 5

# Gobal Dict to store words frequency across threads
globalWordsDict = {}

# FileNames of generated files, with stopwords removed
splitOutputFiles = []

# Every operation requiring a common resource amongst threads, needs to acquire
# lock first
mutex_lock = threading.Lock()


def humanizeFileSize(size):
    """Utility Method to convert filesizes to readable format."""
    size = abs(size)
    if (size == 0):
        return "0B"
    units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB']
    p = math.floor(math.log(size, 2)/10)
    return "%.3f%s" % (size/math.pow(1024, p), units[int(p)])


def searchTOPInHeap(wordsDict, n):
    """Performs Heap search of top n words in dictionary."""
    heap = []
    for key, value in wordsDict.iteritems():
        if len(heap) < n or value > heap[0][0]:
            if len(heap) == n:
                heapq.heappop(heap)
            heapq.heappush(heap, (value, key))
    return heap


def searchLEASTInHeap(wordsDict, n):
    heap = []
    for key, value in wordsDict.iteritems():
        value = -value
        if len(heap) < n or value > heap[0][0]:
            if len(heap) == n:
                heapq.heappop(heap)
            heapq.heappush(heap, (value, key))
    heap = [(-x, y) for x, y in heap]
    return heap


def loadStopWords(filename):
    """Opens stopwords file and writes them to a dictionary."""
    with open(filename, "r") as f:
        for line in f:
            STOPWORDS[line.split()[0].upper()] = True


def makeChunks(iterable, n):
    """makes chunks of n lines from a large number of lines."""
    iterable = iter(iterable)
    while True:
        yield chain([next(iterable)], islice(iterable, n-1))


def shardFile(filename, l):
    """takes a large file and shards it into smaller files"""
    temp = filename.split(".", 1)
    shardedFiles = []
    with open(filename) as bigfile:
        for i, lines in enumerate(makeChunks(bigfile, l)):
            newfileName = '{}{}.{}'.format(temp[0], i+1, temp[1])
            with open(newfileName, 'w') as f:
                f.writelines(lines)
            shardedFiles.append(newfileName)
    return shardedFiles


def processSplitFile(filename):
    """
    Performs several things:
        1) Traverses line by line through smaller file, and calculates word
            frequency.
        2) Removes stopwords in each line
        3) Writes lines to a new file, mapped to each shard.
        4) updates global word count across threads
    """
    temp = filename.split(".", 1)
    fileToWrite = "{}{}.{}".format(temp[0], "_removedStoppedWords", temp[1])
    splitOutputFiles.append(fileToWrite)
    wordsDict = {}
    with open(filename, "r") as f, open(fileToWrite, 'w') as w:
        for l in f:
            line = l.split("\n")[0]
            words = line.split()
            newline = ""
            for word in words:

                # We remove all nonsense characters from each word for
                # frequency and stopwords
                cleanWord = word.strip('"\'[]#,.*()<>?;:').lower()
                if cleanWord in wordsDict:
                    wordsDict[cleanWord] += 1
                else:
                    wordsDict[cleanWord] = 1
                if(cleanWord.upper() not in STOPWORDS):
                    newline += word+" "
            w.write(newline+"\n")
        f.close()
        w.close()

    # Acquire Threads' mutex lock to prevent race conditions.
    with mutex_lock:
        for k, v in wordsDict.iteritems():
            globalWordsDict[k] = globalWordsDict.get(k, 0)+v

if __name__ == "__main__":
    try:
        largeFile = sys.argv[1]
        stopwordsFile = sys.argv[2]
    except IndexError:
        print "Example Usage: \
    'python hs_task1_process_largeTextFile.py shakespear.txt stopwords.txt'"
        sys.exit(1)
    fileSize = humanizeFileSize(os.stat(largeFile).st_size)
    print "Processing %s, filesize %s ..." % (largeFile, fileSize)

    # Loading stopwords in memory
    loadStopWords(stopwordsFile)

    # Creating a list of small files
    shardedFiles = shardFile(largeFile, MAX_FILE_LIMIT)

    # Spawning a thread for each small file, and processing it.
    for f in shardedFiles:
        t = threading.Thread(target=processSplitFile, args=(f,))
        t.start()
        t.join()

    # sorted list of all words in globalWordsDict. Slightly inefficient,
    # compared to heap method.
    l = sorted(globalWordsDict.items(),
               key=operator.itemgetter(1),
               reverse=True)

    print
    print "******************"
    print "Processed. Stats below:"
    print "******************"
    print "Total split files created:{}. Total unique words found:{}.".format(
        len(shardedFiles),
        len(globalWordsDict.keys()))

    print "Top {} most commonly used words:".format(TOP_N_WORDS)
    # for i in l[:TOP_N_WORDS]:
    for i in searchTOPInHeap(globalWordsDict, TOP_N_WORDS):
        print "\t->", i

    print "Top {} least commonly used words:".format(TOP_N_WORDS)
    # for i in l[-TOP_N_WORDS:]:
    for i in searchLEASTInHeap(globalWordsDict, TOP_N_WORDS):
        print "\t->", i

    print
    print "******************"
    print "Generated following files with stop words removed:"
    for i in splitOutputFiles:
        print "\t-> {}".format(i)
