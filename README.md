## Task 1 Execution


```
python hs_task1_process_largeTextFile.py shakespear.txt stopwords.txt
```

## Task 2 Psil evaluation

PSIL evaluator Module
=============

A simple module for evaluating PSIL expressions

Usage from Commandline
-------

Store all the psil expression to be evaluated in psilExpression.txt. Then run:-

	python psilModule.py psilExpressions.txt

Installation
-----------

    python setup.py install

Code:

	import psilModule

	line = "( + 1 ( * 2 3 ) ( / 4 2 (- -5 -10)) ) 	# Expected: 7.4"
	psilValue = psilModule.Psil(line).evaluate()
    print "Value: %s\n"%(psilValue)


Psil Examples
-----------

	# '#' can be used to leave a comment in Psil Expression. Expected: 0
	123 	# Expected: 123
	( + 1 2 (*)) 	# Expected: 3
	( * 5 4 3 2 1) 	# Expected: 120
	( - 10 5 15)	# Expected: -10
	( / -10 5)	# Expected: -2
	( + 1 ( * 2 3 ) ( / 4 2 (- -5 -10)) ) 	# Expected: 7.4
	( bind radius 12 ) 	# Expected: 12
	( bind length 10 ) ( bind breadth 10 ) ( * length breadth )	# Expected: 100
	(( bind length 10 ) ( bind breadth 10 ) ( * length breadth ))	# Expected: Error
	( bind length 10 ) ( + 1 2 3 4 ) ( bind breadth 10 ) ( * length breadth )	# Expected: 100
	( bind length 10 ) ( bind breadth ( + length 1 ) ) ( * length breadth )	# Expected: 110
	( bind a (+ 5 ( bind b (* 2 (bind a 100)))))	# Expected: 205
	( + 1 (* 2 3 (/ 4 3) (* 2 (- 2 3 5 6)) )(- 2 (*) (* 2 3) ))	# Expected: -196
	(+ 1(*))	# Expected: 2
	*	# Expected: 1
	+	# Expected: 0
	/	# Expected: Error
	-	# Expected: Error
	()	#Expected: Error
	( bind 1 12 ) 	# Expected: Error
	( * 5 4 a) 	# Expected: Error
	+ 1 2 3	# Expected: Error
	( )	# Expected: Error
	a	# Expected: Error
	( 1 2 3 4)	# Expected: Error
	+ 1 2	# Expected: Error
	(123)	# Expected: Error
	+ 1 (bind a -10.32 )( bind b (+ a (* 2.12 5.23 a)) )( + a b -4.32 )	# Expected: Error
	(+ 1 (bind a -10.32 )( bind b (+ a (* 2.12 5.23 a)) )( + a b -4.32 ))	# Expected: -273.448
	(+ (* 1 10) 1 (bind a -10.32 )( bind b (+ a (* 2.12 5.23 a)) )( + a b -4.32 ))	# Expected: -263.448
