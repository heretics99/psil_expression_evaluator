#!/usr/bin/python
import operator
import re
import sys


class Psil(object):
    """The Psil class, for all psil related evaluations"""

    # Possible maths symbols
    mathSymbols = {
        "*":   (operator.mul),
        "+":   (operator.add),
        "-":   (operator.sub),
        "/":   (operator.div),
    }

    def __init__(self, psilExpression):
        "Constructor method, to instantiate and tokenize input"

        # Discarding any value after '#', treating it as 'Comments'
        self.psilExpression = psilExpression.split("#", 1)[0]

        # dynamicVars is the dictionary for storing dynamic variables in
        # the expression.
        self.dynamicVars = {}

        # Tokenizing the expression.
        self.tokens = re.sub(r'(\(|\)|\+\s|\-\s|\*\s|\\\s)',
                             r' \1 ', self.psilExpression).split()

    def evaluate(self):
        "Driver method for the class"
        start = 0
        end = len(self.tokens) - 1
        value = self.evaluateChainedPsil(start, end)
        return value

    def isString(self, s):
        "Utility method to check if valid string"
        return re.match("^[A-Za-z_]*$", s)

    def isValidNum(self, s):
        "Utility method to check if valid number"
        try:
            float(s)
            return True
        except:
            return False

    def findMatchingParantheses(self, start):
        """
        Given a starting location, returns the position of corresponding
        matching parenthesis
        """
        l = len(self.tokens)
        nest, i, end = 0, start, start
        if(self.tokens[i] == '('):
            nest += 1
            while nest and i < l:
                i += 1
                if(i == l):
                    raise Exception("Psil Exception: Starting parenthesis \
                        at %s should close around %s" % (start, i))
                if self.tokens[i] == '(':
                    nest += 1
                elif self.tokens[i] == ')':
                    nest -= 1
            end = i
        return end

    def evaluateBalancedPsil(self, start, end, isBeginningOfPsil):
        """
        Responsible for effective evaluation of a psil expression.
        Recursively calls itself, to evaluate sub-expressions.
        Expects a balanced expression.
        Returns a tuple of (evaluated value of current psil, ending position
        of current psil)
        """

        i = start

        # Setting up boundary conditions
        if (end-start < 0):
            return 0, i
        elif (end-start == 0 and self.tokens[i] in "+-*/"):
            if(self.tokens[i] == '+'):
                return 0, i
            elif(self.tokens[i] == '*'):
                return 1, i
            elif(self.tokens[i] == '-'):
                raise Exception("Psil Exception: Invalid token '%s' at \
position: %s" % (self.tokens[start], start,))
            elif(self.tokens[i] == '/'):
                raise Exception("Psil Exception: Invalid token '%s' at \
position: %s" % (self.tokens[start], start,))
            else:
                raise Exception("Psil Exception: Not a valid Psil")

        # Boundary condition to check if this Psil is beginning expression
        # of parent Psil
        if(isBeginningOfPsil):
            if(not (self.tokens[i] in self.mathSymbols or
                    self.tokens[i] == "bind" or
                    self.isValidNum(self.tokens[i]) or
                    self.tokens[i] in self.dynamicVars)):
                raise Exception("Psil Exception: Token '%s' found instead of a \
Psil operator at %s" % (self.tokens[i], i,))

        jump, value = 0, 0

        # Switch cases follow, corresponding to every possible token in
        # Psil Expression

        # Token is '('
        if self.tokens[i] == '(':
            jump = self.findMatchingParantheses(i)
            if(jump == i+1):
                raise Exception("Psil Exception: Empty braces '%s' found at \
position: %s" % (self.tokens[jump], jump,))
            value, jump = self.evaluateBalancedPsil(i+1, jump-1, True)
            i = jump + 1

        # Token is a mathematical symbol
        elif self.tokens[i] in self.mathSymbols:
            op, calc = self.mathSymbols[self.tokens[i]], 0

            # Flag is used to initialize 'calc' with first number (or
            # expression) in series
            flag = True
            while i < end:
                i += 1
                # Nested if-else to check all possible numerical checks
                if self.isValidNum(self.tokens[i]):
                    tokenValue = float(self.tokens[i])
                elif self.isString(self.tokens[i]):
                    if (self.tokens[i] in self.dynamicVars):
                        tokenValue = self.dynamicVars[self.tokens[i]]
                    else:
                        raise Exception("Psil Exception: Undefined Variable \
'%s' at position: %s" % (self.tokens[i], i,))
                elif (self.tokens[i] == '('):
                    tokenValue, jump = self.evaluateBalancedPsil(
                        i+1,
                        self.findMatchingParantheses(i)-1, True)
                    i = jump + 1
                if(flag is True):
                    calc = tokenValue
                    flag = False
                else:
                    calc = op(calc, tokenValue)
            value = calc

        # Token is "bind" operator
        elif self.tokens[i] == "bind":
            value, jump = self.evaluateBalancedPsil(
                i+2,
                self.findMatchingParantheses(i+2), False)
            if(self.isString(self.tokens[i+1])):
                self.dynamicVars[self.tokens[i+1]] = value
            else:
                raise Exception("Psil Exception: assignment can only be done \
to a string, not '%s' at position: %s" %
                                (self.tokens[i+1], i+1,))
            i = jump

        # Token is a valid number
        elif self.isValidNum(self.tokens[i]):
            value = float(self.tokens[i])

        # Token is a valid pre-existing variable
        elif self.tokens[i] in self.dynamicVars:
            value = self.dynamicVars[self.tokens[i]]

        # Raise error if none of this is satisfied
        else:
            raise Exception("Psil Exception: Invalid token '%s' at \
position: %s" % (self.tokens[i], i,))

        # Return value, location tuple
        return value, i

    def evaluateChainedPsil(self, start, end):
        """To iteratively evaluate Psil expressions of the form- ()()()..."""

        # A Boundary check for sanity
        if(len(self.tokens) > 1 and self.tokens[0] is not '('):
            raise Exception("Psil Exception: First token should be a \
parenthesis instead of '%s'" % (self.tokens[0],))

        # First call to evaluate Psil from First location
        value, jump = self.evaluateBalancedPsil(start, end, False)
        jump += 1

        # Evaluating other remaining Psils after the first one.
        while jump < end:
            value, jump = self.evaluateBalancedPsil(jump, end, False)
            jump += 1
        return value


# If program is directly used on command line, instead of module
if __name__ == "__main__":
    try:
        filename = sys.argv[1]
    except IndexError:
        print "Usage: python psilModule.py psilExpressions.txt"
        sys.exit(1)

    with open(filename, "r") as f:
        for i, l in enumerate(f):
            line = l.split("\n")[0]
            print "[%s]" % (i+1)
            print line
            try:
                psilValue = Psil(line).evaluate()
                print "Value: %s\n" % (psilValue)
            except Exception, e:
                print "%s\n" % e
                # traceback.print_exc()
