from setuptools import setup, find_packages
setup(
    name="Psil",
    version="0.1",
    author="Prakhar Goel",
    author_email="i@prakhargoel.com",
    description="Psil Expressions evaluator",
    long_description=open("README.md").read(),
    packages=find_packages(),
    scripts=['psilModule.py'],
)
