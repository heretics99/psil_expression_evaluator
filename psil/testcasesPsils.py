import unittest
import sys
import psilModule
import unittest


class PsilTests(unittest.TestCase):
    pass


def genericTestFunction(actual, expected, description):
    def test(self):
        if(isinstance(expected, float)):
            self.assertAlmostEqual(actual, expected, places=3, msg=description)
        else:
            self.assertEqual(str(actual), str(expected), msg=description)
    return test

if __name__ == '__main__':
    filename = "psilExpressions.txt"
    with open(filename, "r") as f:
        for i, l in enumerate(f):
            line = l.split("\n")[0]
            try:
                expected = float(line.split("Expected: ")[1])
            except:
                expected = "Error"

            detailedError = ""
            try:
                actualPsilValue = psilModule.Psil(line).evaluate()
            except Exception, e:
                actualPsilValue = "Error"
                detailedError = str(e)

            test_name = "{}".format(i+1)
            assertion_description = "Assertion Failed:: Expected value: '{0}'. Actual value: {1}".format(expected, actualPsilValue)
            test_func = genericTestFunction(actualPsilValue, expected, assertion_description)

            setattr(PsilTests, 'test_{0}'.format(test_name), test_func)
            print "[{}] {}".format(i+1, line)
            print "Value: {}".format(actualPsilValue)
            if len(detailedError) > 0:
                print "{}".format(detailedError)
            print

    unittest.main()
