import psilModule

line = "( + 1 ( * 2 3 ) ( / 4 2 (- -5 -10)) )   # Expected: 7.4"
psilValue = psilModule.Psil(line).evaluate()
print "Value: %s\n" % (psilValue)
